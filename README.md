**Lafayette 24 hour urgent care**

The 24-hour emergency treatment center Lafayette is a recently remodeled and enlarged facility that provides 
our patients with state-of-the-art facilities and emergency experience. 
We're operating a 24-hour emergency room with 20 beds. 
In an emergency situation, we are in a position to offer the appropriate treatment to stabilize people of all ages.
Our Lafayette Haven 24-hour emergency services and ancillary equipment are also available 24-hours a day, such as 
laboratory, CT, X-ray, ultrasound and respiratory therapy. We are proud of customer service and of building a positive therapeutic environment. 
UHC has received Press Ganey's coveted Guardian of Excellence Award for Patient Experience.
Please Visit Our Website [Lafayette 24 hour urgent care](https://urgentcarelafayette.com/24-hour-urgent-care.php) for more information. 

---

## Our 24 hour urgent care in Lafayette services

Lafayette's 24-hour emergency service is committed to delivering the highest quality of health care. 
Both cases reported to the Emergency Room will be assessed without respect to color, colour, religion or desire of the individual to pay.
Appropriate medical care will be delivered by qualified nursing professionals, medical personnel and ancillary resources 
when appropriate to provide as good a patient experience as possible. 
If a higher level of care is sufficient, we can move to the more acceptable hospital.

